//Server should be implemented here
const express = require('express');
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const port = 3000;

app.listen(port, function() {
    console.log('listening to port:', port);
})

module.exports = app;
