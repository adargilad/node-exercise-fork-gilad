#Api documentation

####Description:
Get all series information includes all series seasons and their episodes.
This action will use join the the SQL statement.

####Method: /GET

####Endpoint: /fetchSeriesWithJoins

####Response with success:

```
[
  {
    seriesID: 1,
    name: "Game of Phones",
    year: "2009-12-31T22:00:00.000Z",
    director: "James Hamron",
    genre: "Fantasy",
    seasons: [
      {
        seasonID: 1,
        seasonNumber: 1,
        airDate: "2010-01-01T18:00:37.000Z",
        episodes: [
          {
            episodeID: 1,
            episodeNumber: 1,
            airDate: "2010-01-01T18:00:37.000Z"
          }
        ]
      }
    }
 ]
```

####Response with error:

```
{
  "message": "Error getting data."
}
```

####Description:
Get all series information includes all series seasons and their episodes.
This action will not use join the the SQL statement.

####Method: /GET

####Endpoint: /fetchSeriesNoJoins

####Response with success:

```
[
  {
    seriesID: 1,
    name: "Game of Phones",
    year: "2009-12-31T22:00:00.000Z",
    director: "James Hamron",
    genre: "Fantasy",
    seasons: [
      {
        seasonID: 1,
        seasonNumber: 1,
        airDate: "2010-01-01T18:00:37.000Z",
        episodes: [
          {
            episodeID: 1,
            episodeNumber: 1,
            airDate: "2010-01-01T18:00:37.000Z"
          }
        ]
      }
    }
 ]
```

####Response with error:

```
{
  "message": "Error getting data."
}
```

####Description:
Add a user the episodes watch list table and rate the episode

####Method: /POST

####Endpoint: /addUserWatchData/userID

####Post parameters:
######episodeID (Int) - mandatory, stars (Int) - not mandatory

####Response with success:

```
{
    "message": "new data inserted"
}
```

####Response with error without supplying mandatory parameters:

```
{
    "message": "Mandatory parameters were not supplied."
}
```

####Response with server error:

```
{
    "message": "Error inserting new data."
}
```

####Description:
Get all series information that a user watched.

####Method: /GET

####Endpoint: //fetchUserWatchHistory/userID

####Response with success:

```
[
  {
    seriesID: 1,
    name: "Game of Phones",
    year: "2009-12-31T22:00:00.000Z",
    director: "James Hamron",
    genre: "Fantasy",
    seasons: [
      {
        seasonID: 1,
        seasonNumber: 1,
        airDate: "2010-01-01T18:00:37.000Z",
        episodes: [
          {
            episodeID: 1,
            episodeNumber: 1,
            airDate: "2010-01-01T18:00:37.000Z"
          }
        ]
      }
    }
 ]
```

####Response with error:

```
{
  "message": "Error getting data."
}
```
