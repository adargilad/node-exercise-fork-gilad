const mysql = require('mysql');

module.exports = function() {

  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'TvShows'
  });

  connection.connect(function(err) {
    if (err) {
      //Handle this error
      return console.log('error connecting to mySql:', err);
    }
    else {
        console.log('connected!');
    }
  });

  //Wrapper method to execute queries with callbacks
  const executeQueryWithCallback = function(query, callback) {
    connection.query(query, callback);
  };

  //Wrapper method to execute queries with promises
  const executeQueryWithPromise = function(query) {
    return new Promise(function(resolve, reject) {
      connection.query(query, function(err, results, fields) {
        if (err)
          return reject(err);
        resolve(results);
      });
    });
  };

  const db = {};

  const seriesToJson = function(res) {
      let serieses = [];

      //Iterate the all series
      if (res.length > 0) {
          res.forEach((item, index) => {
              //Check if series exists in serieses new array
              const series = serieses.filter((x) => {
                  return x.seriesID === item.seriesID;
              })

              //If not exists in array, push the series to the array
              if (series.length === 0) {
                  serieses.push({
                      seriesID: item.seriesID,
                      name: item.name,
                      year: item.year,
                      director: item.director,
                      genre: item.genre,
                      seasons: []
                  })
              }

              //Get seasonID index;
              const seriesIndex = serieses.findIndex(x => x.seriesID === item.seriesID);

              //Check if season exists in series
              const season = serieses[seriesIndex].seasons.filter((x) => {
                  return x.seasonNumber === item.seasonNumber;
              })

              //If season not exists in the series push the season to the array
              if (season.length === 0) {
                  serieses[seriesIndex].seasons.push({
                      seasonID: item.seasonID,
                      seasonNumber: item.seasonNumber,
                      airDate: item.airDate,
                      episodes: []
                  })
              }

              //Get seasonIndex
              const seasonIndex = serieses[seriesIndex].seasons.findIndex(x => x.seasonNumber === item.seasonNumber);

              //Check if episode exists in season in a series
              const episode = serieses[seriesIndex].seasons[seasonIndex].episodes.filter((x) => {
                  return x.episodeNumber === item.episodeNumber;
              })

              //If the episode is not exists in the season of the series, push in to the array
              if (episode.length === 0) {
                  serieses[seriesIndex].seasons[seasonIndex].episodes.push({
                      episodeID: item.episodeID,
                      episodeNumber: item.episodeNumber,
                      airDate: item.airDate
                  });
              }
          })
      }

      return serieses;
  }

  db.fetchSeriesNoJoins = function(callback) {
    //Get total seasons to know when execute the callback
    let totalSeasons;
    const query = "SELECT COUNT(*) as total FROM seasons";

    executeQueryWithPromise(query).then((res) => {
        totalSeasons = res[0].total;
    })
    .then(() => {
        //Get the series from the db.
        let serieses = [];
        const query = "SELECT DISTINCT id as seriesID, name, year, director, genre FROM series";

        //Push all searies from the db to the array
        executeQueryWithPromise(query).then((res) => {
            res.forEach((item, index) => {
                serieses.push({
                    seriesID: item.seriesID,
                    name: item.name,
                    year: item.year,
                    director: item.director,
                    genre: item.genre,
                    seasons: []
                })
            })

            //Iterate all searies from the db
            serieses.forEach((item, index) => {
                //For each series get its seasons from the db.
                const query = "SELECT id as seasonID, seasonNumber, airDate FROM seasons WHERE seriesID = " + item.seriesID;

                executeQueryWithPromise(query).then((res) => {
                    const seriesIndex = serieses.findIndex(x => x.seriesID === item.seriesID);

                    //Push each season to its series
                    res.forEach((item) => {
                        serieses[seriesIndex].seasons.push({
                            seasonID: item.seasonID,
                            seasonNumber: item.seasonNumber,
                            airDate: item.airDate,
                            episodes: []
                        })
                    })

                    //Iterate all seasons of a series
                    serieses[seriesIndex].seasons.forEach((item, index) => {
                        //For each season of a series get its episodes from the db.
                        const seasonIndex = serieses[seriesIndex].seasons.findIndex(x => x.seasonID === item.seasonID);
                        const query = "SELECT id as episodeID, episodeNumber, airDate FROM episodes WHERE seasonID = " + item.seasonID;

                        //Push all episodes of a season of a series to the array
                        executeQueryWithPromise(query).then((res) => {
                            res.forEach((item) => {
                                serieses[seriesIndex].seasons[seasonIndex].episodes.push({
                                    episodeID: item.episodeID,
                                    episodeNumber: item.episodeNumber,
                                    airDate: item.airDate
                                })
                            })

                            //Check if all seasons were handled and exit
                            if (item.seasonID === totalSeasons) {
                                callback(null, serieses);
                            }
                        })
                    })
                }).catch(err => {
                    console.log(err);

                    callback(err);
                })
            })
        }).catch(err => {
            console.log('fetchSeriesWithJoins error:', err);

            callback(err);
        })
    })
  };

  db.fetchSeriesWithJoins = function(callback) {
    // Add code here
    const query = `SELECT series.id as seriesID, series.name, series.year,
                          series.director, series.genre, seasons.id as seasonID, seasons.seasonNumber,
                          seasons.airDate, episodes.id as episodeID, episodes.episodeNumber, episodes.airDate
                   FROM series
                   INNER JOIN seasons ON series.id = seasons.seriesID
                   INNER JOIN episodes ON episodes.seasonID = seasons.id
                   ORDER BY series.id, seasons.id, episodes.id`

    executeQueryWithPromise(query).then((res) => {
        const serieses = seriesToJson(res);

        callback(null, serieses);
    }).catch(err => {
        console.log('fetchSeriesWithJoins error:', err);

        callback(err);
    })
  };

  db.addUserWatchData = function(data, callback) {
    // Add code here
    const { userID, episodeID, rate } = data;
    const query = "INSERT INTO usersWatchedHistory (id, userID, episodeID, rate, InsertDate) VALUES (null, " + userID + ", " + episodeID + ", " + rate + ", now())";

    executeQueryWithPromise(query).then((res) => {
        callback(null);
    }).catch(err => {
        console.log('addUserWatchData', err);

        callback(err);
    })
  };

  db.fetchUserWatchHistory = function(data, callback) {
    // Add code here
    const userID = data.userID;

    const query = `SELECT series.id as seriesID, series.name, series.year,
                          series.director, series.genre, seasons.id as seasonID, seasons.seasonNumber,
                          seasons.airDate, episodes.id as episodeID, episodes.episodeNumber, episodes.airDate
                   FROM series
                   INNER JOIN seasons ON series.id = seasons.seriesID
                   INNER JOIN episodes ON episodes.seasonID = seasons.id
                   INNER JOIN usersWatchedHistory history ON history.episodeID = episodes.id
                   INNER JOIN users ON users.id = history.userID
                   WHERE history.userID = ` + userID + `
                   ORDER BY series.id, seasons.id, episodes.id`

    executeQueryWithPromise(query).then((res) => {
        const serieses = seriesToJson(res);

        callback(null, serieses);
    }).catch(err => {
        console.log('fetchUserWatchHistory error:', err);

        callback(err);
    })
  };

  return db;
};
