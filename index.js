//App should start here!
const app = require('./server/server');
const db = require('./db/db');
const sql = new db();

app.get('/fetchSeriesNoJoins', (req, res) => {
    sql.fetchSeriesNoJoins(function(err, dbRes) {
        if (err) {
            res.status(500).json({
                message: 'Error getting data.'
            })
        }
        else {
            res.json({
                data: dbRes
            })
        }
    })
});

app.get('/fetchSeriesWithJoins', (req, res) => {
    sql.fetchSeriesWithJoins(function(err, dbRes) {
        if (err) {
            res.status(500).json({
                message: 'Error getting data.'
            })
        }
        else {
            res.json({
                data: dbRes
            })
        }
    })
});

app.post('/addUserWatchData/:userID', (req, res) => {
    const data = {
        userID: req.params.userID,
        episodeID: req.body.episodeID,
        rate: req.body.stars || -1 // -1 (user didnt rate the episode)
    }

    //Validate post params (rate is not mandatory)
    if (!data.episodeID) {
        res.status(422).json({
            message: 'Mandatory parameters were not supplied.'
        })
    }
    else {
        sql.addUserWatchData(data, function(err, dbRes) {
            if (err) {
                res.status(500).json({
                    message: 'Error inserting new data.'
                })
            }
            else {
                res.json({
                    message: 'new data inserted'
                })
            }
        })
    }
});

app.get('/fetchUserWatchHistory/:userID', (req, res) => {
    const data = {
        userID: req.params.userID
    }

    sql.fetchUserWatchHistory(data, function(err, dbRes) {
        if (err) {
            res.status(500).json({
                message: 'Error getting data.'
            })
        }
        else {
            res.json({
                data: dbRes
            })
        }
    })
});

module.exports = app;
